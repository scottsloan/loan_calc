# loan_calc

A simple loan calculator written in rust.

## Usage

```bash
Usage: loan_calc.exe [OPTIONS] --amount <AMOUNT> --rate <RATE> --term <TERM>

Options:
      --amount <AMOUNT>  Loan Amount
      --rate <RATE>      APR of Loan as a Percent (i.e 5.0 for 5%)
      --term <TERM>      Term of the loan in months
      --schedule         Prints the Ammortization Schedule
  -h, --help             Print help
  -V, --version          Print version
```


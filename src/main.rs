use clap::Parser;

#[derive(Parser, Debug)]
#[command(author ="Scott Sloan <scottsloan@gmail.com>", version ="0.1.0", about ="Simple loan calculator that will also do ammortization schedules", long_about = None)]
struct Cli {
    #[arg(long)]
    amount : f64,
    #[arg(long)]
    rate : f64,
    #[arg(long)]
    term : i32,
    #[arg(long)]
    schedule : bool
}

fn calc_payment(principal: f64, APR : f64, loan_term : i32) -> f64{
    // P = a/(((1+r)^n)-1)/(r*(1_r)^n)
    // P = Monthly loan payment
    // a = principal
    // r = periodic interest rate (interest_rate / 12 months)
    // n = number of months in your loan term
    // but, lets break it down t0 : P = a / (x / y)
    let r = APR / 12.0;
    let x = f64::powi(1.0+r, loan_term) - 1.0;
    let y = (x+1.0) * r; 
    let p = principal / (x / y);

    //return value
    p
}

fn print_ammortization_schedule(principal:f64, apr:f64, loan_terms:i32) {
    let mut balance = principal;

    let payment = calc_payment(principal, apr, loan_terms);

    for n in 1..=loan_terms {
        let cur_interest = principal * (apr / 12.0);
        let new_balance = balance + cur_interest - payment;
        println!("{} \t\t ${:.2} \t\t ${:.2} \t\t ${:.2} \t\t ${:.2}", n, balance, cur_interest, payment, new_balance);
        balance = new_balance;
    }
}

fn main() {
    let cli = Cli::parse();

    let loan_terms_months : i32 = cli.term; 
    let principal  = cli.amount;
    let apr = cli.rate / 100.0;

    let payment        = calc_payment(principal, apr, loan_terms_months);
    let total_paid     = payment * loan_terms_months as f64;
    let interest_paid  = total_paid - principal;

    println!("Loan Details");
    println!("------------");
    println!("Loan Amount : ${:.2}", principal);
    println!("Interest Rate : {:.2} %", cli.rate );
    println!("Loan Term : {:.2} months", loan_terms_months);

    println!("Payment : ${:.2}", payment);
    println!("Interest Paid : ${:.2}", interest_paid);
    println!("Total Paid : ${:.2}", total_paid);

    if cli.schedule
    {
        println!("Loan Ammortization Schedule");
        println!("---------------------------");
        print_ammortization_schedule(principal, apr, loan_terms_months);
    }
}
